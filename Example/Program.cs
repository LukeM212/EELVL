﻿using System;
using EELVL;

namespace Example
{
	using static Blocks;

	class Program
	{
		static void Main(string[] args)
		{
			Console.Write("File name: ");
			string filename = Console.ReadLine();

			Console.WriteLine();

			if (filename.ToLower().EndsWith(".eelvl")) LevelExample(filename);
			else if (filename.ToLower().EndsWith(".eelvls")) BundleExample(filename);
			else Console.WriteLine("Unknown file type");

			Console.WriteLine();

			Console.Write("Press [ENTER] to close.");
			Console.ReadLine();
		}

		static void LevelExample(string filename)
		{
			Level lvl = Level.Open(filename);

			Console.WriteLine($"      World Name: {lvl.WorldName}");
			Console.WriteLine($"     Description: {lvl.Description}");
			Console.WriteLine($"           Width: {lvl.Width}");
			Console.WriteLine($"          Height: {lvl.Height}");
			Console.WriteLine($"      Owner Name: {lvl.OwnerName}");
			Console.WriteLine($"        Owner ID: {lvl.OwnerID}");
			Console.WriteLine($"       Crew Name: {lvl.CrewName}");
			Console.WriteLine($"         Crew ID: {lvl.CrewID}");
			Console.WriteLine($"     Crew Status: {lvl.CrewStatus}");
			Console.WriteLine($"Background Color: {lvl.BackgroundColor}");
			Console.WriteLine($"         Gravity: {lvl.Gravity}");
			Console.WriteLine($"         Minimap: {lvl.Minimap}");
			Console.WriteLine($"        Campaign: {lvl.Campaign}");
			Console.WriteLine();
			Console.WriteLine($"The foreground ID at (12, 10) was {lvl[0, 10, 10].BlockID}");
			Console.WriteLine($"The background ID at (3, 7) was {lvl[1, 3, 7].BlockID}");

			Console.WriteLine();

			lvl[0, 10, 10] = new PortalBlock(242, 0, 0, 42);
			lvl[1, 3, 7] = new Block(500);

			Console.Write("Save as: ");
			filename = Console.ReadLine();

			lvl.Save(filename);
		}

		static void BundleExample(string filename)
		{
			LevelBundle bundle = LevelBundle.Open(filename);

			foreach (LevelBundleEntry entry in bundle)
				Console.WriteLine($"{entry.WorldID} ({entry.Filename}) - {entry.Level.WorldName} by {entry.Level.OwnerName}");

			Console.WriteLine();

			LevelBundleEntry defaultWorld = bundle[^1];
			bundle.RemoveAt(bundle.Count - 1);
			bundle.Insert(0, defaultWorld);

			Console.Write("Save as: ");
			filename = Console.ReadLine();

			bundle.Save(filename);
		}
	}
}
